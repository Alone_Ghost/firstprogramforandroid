import QtQuick 2.0
import QtQuick.Controls 2.12

Item {
    property var setNamButton: "Forget Set"
    property var setSizeText: "9"
    property var setColorButton
    property var setColorText: "white"
    property var setRoundRectangle: "0.04"

    Rectangle{
        anchors.fill: parent
        color: setColorButton
        Text {
            text: setNamButton
            color: setColorText
            font.pixelSize: setSizeText
            anchors.centerIn: parent
        }
        radius: parent.height*setRoundRectangle
    }
}
