import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "PagesComplete"


ApplicationWindow {
    width: 1080
    height: 1920
    visible: true

    property bool visiablePageFirst: true
    property var resultText:"Your Result is: "
    StackView{
        id:stackView
        initialItem: pageMain
        width: parent.width
        height: parent.height*0.1
        anchors.centerIn: parent

    }

    Loader{
        id:loader
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
    }
    Component{
        id:pageMain
        InputNumbers{
            id:inputNumbers
            visible: visiablePageFirst
            onSwitchToAnotherPage: {
                visiablePageFirst = false
                stackView.push(pageShowResult)
                resultText += resultTextSend

            }

        }

    }

    Component{
        id:pageShowResult
        PageShowResult{
            restultTextForm:resultText
            Keys.onReleased: {
                if (event.key === Qt.Key_Back) {
                    console.log("shod")
                    event.accepted = true
                    stackView.push(pageMain)
                    resultText="Your Result is:"
                }
            }


        }
    }

}



/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/
