import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "../PartComponets"

Item {
    id:itemPage
    property var resultTextSend

    signal switchToAnotherPage()

    TextField{
        id:textFiledFirstInput
        width: parent.width*0.35
        height: parent.height*0.48
        placeholderText: "First Number"
        anchors.left: parent.left
        inputMethodHints: Qt.ImhDigitsOnly

    }

    Label{
        width: parent.width*0.3
        height: parent.height*0.48
        Text {
            text: "+"
            font.pixelSize: 35
            anchors.centerIn: parent
            color: "#607D8B"
        }


        anchors.left: textFiledFirstInput.right
        anchors.leftMargin: parent.width*0.05
        anchors.top: parent.top

    }

    TextField{
        id:textFiledSecondInput
        width: parent.width*0.35
        height: parent.height*0.48
        placeholderText: "Second Number"
        anchors.right: parent.right
        inputMethodHints: Qt.ImhDigitsOnly

    }
    CustomButtons{
        id:resultButton
        width: parent.width*0.2
        height: parent.height*0.3
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        setColorText: "white"
        setNamButton: "Result"
        setSizeText: "30"
        setRoundRectangle: "0.2"
        setColorButton: "#607D8B"
        MouseArea{
            anchors.fill: parent
            onPressed: {
                resultButton.setColorButton="white"
                resultButton.setColorText = "#607D8B"
                resultTextSend = parseInt(textFiledFirstInput.text) + parseInt(textFiledSecondInput.text);
                itemPage.switchToAnotherPage()


            }
            onReleased: {
                resultButton.setColorButton="#607D8B"
                resultButton.setColorText = "white"
            }

        }
    }


}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.75;height:480;width:640}
}
##^##*/
